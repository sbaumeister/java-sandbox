package com.sbaumeister.model;

import com.sbaumeister.model.annotations.Attribute;

import java.math.BigInteger;

public class Alcohol {

    class Code {}
    class LanguageCode {}
    class AttributeGroupValues {}
    class MultilingualValue {}

    @Attribute(id="M101")
    private BigInteger percentage;

    @Attribute(id="M101", codelist="ZAS_BLUB")
    private Code blubCode;

    @Attribute(id="M102", multilingual=true)
    private String text;

/*    @AttributeGroup({
        @Attribute(id="M202"),
        @Attribute(id="M203")
    })*/
    private AttributeGroupValues items;

}
