package com.sbaumeister.model;


import com.sbaumeister.model.annotations.Attribute;
import com.sbaumeister.model.annotations.AttributeGroup;

@AttributeGroup
public class ArticleDescription {

    @Attribute
    String attr1;

    @Attribute
    String attr2;
}
