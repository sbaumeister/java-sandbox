package com.sbaumeister.model;

import com.sbaumeister.model.annotations.Attribute;
import com.sbaumeister.model.annotations.AttributeValue;

@Attribute(id = "M105")
public class UnitDescriptor {

    @AttributeValue
    String value;
}
