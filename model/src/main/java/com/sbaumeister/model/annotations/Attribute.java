package com.sbaumeister.model.annotations;

public @interface Attribute {
    String id() default "";

    String codelist() default "";

    boolean multilingual() default false;
}
