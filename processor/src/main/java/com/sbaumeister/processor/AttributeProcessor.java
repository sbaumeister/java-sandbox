package com.sbaumeister.processor;

import com.google.auto.service.AutoService;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.TypeSpec;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.util.Set;

@AutoService(Processor.class)
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedAnnotationTypes("com.sbaumeister.model.annotations.*")
public class AttributeProcessor extends AbstractProcessor {

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

        for (TypeElement annotation : annotations) {
            Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(annotation);
            for (Element element : elements) {
                if (element.getKind().isClass()) {

                    PackageElement packageElement = processingEnv.getElementUtils().getPackageOf(element);

                    String className = element.getSimpleName() + "Attribute";
                    TypeSpec attributeClass = TypeSpec.classBuilder(className)
                            .addModifiers(Modifier.PUBLIC)
                            .build();
                    JavaFile javaFile = JavaFile.builder(packageElement.getQualifiedName().toString(), attributeClass)
                            .build();

                    try {
                        javaFile.writeTo(processingEnv.getFiler());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return true;
    }
}
